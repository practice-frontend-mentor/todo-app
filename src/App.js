import './App.css';
import InputTodo from './components/InputTodo'
import Tarea from './components/Tarea';
import Filtro from './components/Filtro';
import { useState } from 'react';

function App() {

  const [tareas, setTareas] = useState([])
  const [filtro, setFiltro] = useState("all")
  const [tareasFil,setTareasFil] = useState([...tareas])
  const [dark, setDark] = useState(false)

  const agregarTarea= tarea => {
    if(tarea.text.trim()){
      tarea.text=tarea.text.trim();
      setTareas([...tareas,tarea])
      setTareasFil([...tareas,tarea])
    }
  }

  const completar = id => {
    const tareasActualizadas = tareas.map(tarea => {
      if(tarea.id===id){
        tarea.completada=!tarea.completada
      }
      return tarea
    })
    setTareas(tareasActualizadas)
  }

  const borrar = id => {
    const tareasActualizadas = tareas.filter(tarea => tarea.id!==id)
    setTareas(tareasActualizadas)
    setTareasFil(tareasActualizadas)
  }


  const all = () => {
    setFiltro('all')
    setTareasFil([...tareas])
  }

  const active = () => {
    setFiltro('active')
    setTareasFil([...tareas])
    setTareasFil(tareas.filter(tarea => tarea.completada===false))
  }

  const complete = () => {
    setFiltro('completed')
    setTareasFil([...tareas])
    setTareasFil(tareas.filter(tarea => tarea.completada===true))
  }

  const clearComplete = () => {
    const tareasActualizadas = tareas.filter(tarea => tarea.completada!==true)
    setTareas(tareasActualizadas)
    setTareasFil(tareasActualizadas)
    setFiltro("all")
  }

  const darkMode = () => {
    const body=document.body;
    if(dark===false){
      setDark(!dark)
      body.className="noct"
    }
    else{
      setDark(!dark)
      body.classList.remove('noct')
    }
  }

  return (
    <main className="App">
      <header className='header'>
        <h1>T O D O</h1>
        <button className='noct-btn' onClick={darkMode}></button>
      </header>
      <section className='todo-list'>
        <InputTodo onSubmit={agregarTarea}/>
        <section className='list-tareas'>
          <div>
            {tareasFil.map(tarea=>
              <Tarea
                key={tarea.id}
                id={tarea.id}
                texto={tarea.text}
                completada={tarea.completada}
                completar={completar}
                borrar={borrar}
              />
            )}
            
          </div>

          <Filtro 
            tareas={tareasFil}
            filtroType={filtro}
            all={all}
            active={active}
            complete={complete}
            clrComplete={clearComplete}
          />
        </section>
      </section>
    </main>
  );
}

export default App;
