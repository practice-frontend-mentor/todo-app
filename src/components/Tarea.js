import check from '../img/icon-check.svg'
import '../css/Tarea.css'
import cross from '../img/icon-cross.svg'

function Tarea( { id, texto, completada, completar, borrar }){
  
  return(
    <div >
      <div className="tarea-container">
        <div onClick={() => completar(id)}>
          {completada
          ?
            <>
              <div className="boxcheck-l complete">
                <img src={check} />
              </div>
              <p className="complete-p">{texto}</p>
            </>
          :
            <>
              <div className="boxcheck-l">
              </div>
              <p>{texto}</p>
            </>
          }
        </div>
        <img src={cross} className="btn-borrar" onClick={()=>borrar(id)}/>
      </div>
    </div>
  )
}

export default Tarea;