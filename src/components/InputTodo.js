import React, { useState } from "react";
import '../css/InputTodo.css';
import { v4 as uuidv4 } from 'uuid';

function InputTodo(props){

  const [input,setInput] = useState('');

  const capturarTarea = e =>{
    setInput(e.target.value);
  }

  const envioTarea = e =>{
    e.preventDefault();
    
    const nuevaTarea ={
      id: uuidv4(),
      text: input,
      completada: false
    }

    props.onSubmit(nuevaTarea)
  }



  return(
    <form onSubmit={envioTarea}>
      <div className="boxcheck" onClick={envioTarea}></div>
      <input 
        type="text"
        className="input-todo"
        placeholder="Create a new todo..."
        onChange={capturarTarea} />
    </form>
  )
}

export default InputTodo;