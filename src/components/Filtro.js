import React from "react";
import '../css/Filter.css'

function Filtro ( {tareas,filtroType, all, active, complete, clrComplete} ) {
    return(
    <section className="container-filter">
        <p>{tareas.length} items left</p>
        <div className="container-filter-btn">
            {filtroType=="all"?<p className="btn selected" onClick={() => all()}>All</p>:<p className="btn" onClick={() => all()}>All</p>}
            {filtroType=="active"?<p className="btn selected" onClick={() => active()}>Active</p>:<p className="btn" onClick={() => active()}>Active</p>}
            {filtroType=="completed"?<p className="btn selected" onClick={() => complete()}>Completed</p>:<p className="btn" onClick={() => complete()}>Completed</p>}
        </div>
        <p className="btn" onClick={() => clrComplete()}>Clear Completed</p>
    </section>
    )
}   

export default Filtro;